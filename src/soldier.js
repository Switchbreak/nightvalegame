EPSILON					= 0.001;
MOVE_SPEED				= 200;
SOLDIER_COOLDOWN_TIME	= 0.1;
AI_FIRE_RANGE_SQ		= 90000;

var Soldier = Unit.extend({
	MaxHP: 50,
	progressBar: null,
	isBuilding: false,
	inCoolDown: false,
	isInvulnerable: false,
	name: "Soldier",
	
	walkBackAnim: null,
	walkFrontAnim: null,
	walkSideAnim: null,
	currentAnim: null,
	
	ctor: function () {
		cc.spriteFrameCache.addSpriteFrames(res.Soldier_plist);
		var walkBackFrames = [];
		var walkFrontFrames = [];
		var walkSideFrames = [];
		
		for( var i = 1; i <= 5; i++ ) {
			walkBackFrames.push( cc.spriteFrameCache.getSpriteFrame("Soldier Back " + i + ".png") );
			walkFrontFrames.push( cc.spriteFrameCache.getSpriteFrame("Soldier Front " + i + ".png") );
			walkSideFrames.push( cc.spriteFrameCache.getSpriteFrame("Soldier Side " + i + ".png") );
		}
		walkSideFrames.push( cc.spriteFrameCache.getSpriteFrame("Soldier Side 6.png") );
		
		this.walkBackAnim = new cc.RepeatForever( new cc.Animate( new cc.Animation( walkBackFrames, 0.1 ) ) );
		this.walkFrontAnim = new cc.RepeatForever( new cc.Animate( new cc.Animation( walkFrontFrames, 0.1 ) ) );
		this.walkSideAnim = new cc.RepeatForever( new cc.Animate( new cc.Animation( walkSideFrames, 0.1 ) ) );
		
		this._super( "#Soldier Front 1.png" );
		this.anchorY = 0.10;
        this.scheduleUpdate();
		
		this.progressBar = cc.ProgressTimer.create(new cc.Sprite(res.ProgressBar_png));
		this.progressBar.type = cc.ProgressTimer.TYPE_RADIAL;
		this.progressBar.percentage = 0;
		this.progressBar.visible = false;
		this.progressBar.setPosition( this.x + this.width / 2, this.y + this.height / 2 );
		this.addChild(this.progressBar);
	},
	
	update: function ( dt ) {
		if( !this.isBuilding ) {
			this._super( dt );
			
			if( this.isSelected ) {
				this.weaponInput();
				if( SharedData.keyboard[cc.KEY.c] )
					this.startBuild();
			}
		}
	},
	
	weaponInput: function() {
		if( !this.inCoolDown && SharedData.isLeftMouseDown ) {
			this.fire( SharedData.gameLayer.convertToNodeSpace( SharedData.mouseLocation ) );
		}
	},
	
	startBuild: function() {
		var pos = this.getPosition();
		var tileLayer = SharedData.collisionLayer;
		var tilePos = CollisionUtil.getTilePosition(pos, tileLayer);
		
		if( CollisionUtil.isTileSolid(tilePos, tileLayer, true ) )
			return;
		
		this.isBuilding = true;
		this.progressBar.runAction( new cc.Sequence( new cc.ProgressTo(3, 100), new cc.CallFunc( this.buildBunker, this ) ) );
		this.progressBar.visible = true;
	},
	
	buildBunker: function() {
		var pos = this.getPosition();
		var tileLayer = SharedData.collisionLayer;
		var tilePos = CollisionUtil.getTilePosition(pos, tileLayer);
		
		if( CollisionUtil.isTileSolid(tilePos, tileLayer, true ) )
			return;
		
		var buildingPos = tileLayer.getPositionAt(tilePos);
		buildingPos.x += tileLayer.tileWidth / 2;
		buildingPos.y += tileLayer.tileHeight / 2;
		
		var bunker = SharedData.gameLayer.spawnGameObject("Bunker", buildingPos.x, buildingPos.y);
		SharedData.dijkstraMap = CollisionUtil.getDijkstraMap(true);
		
		if( SharedData.selectedUnit == this )
			SharedData.gameLayer.selectUnit(bunker);
		SharedData.gameLayer.removeUnit(this);
	},
	
	playWalkAnimation: function( vector ) {
		var animation;
		
		if( Math.abs( vector.y ) > Math.abs( vector.x ) ) {
			if( vector.y > 0 )
				animation = this.walkBackAnim;
			else
				animation = this.walkFrontAnim;
		}
		else {
			animation = this.walkSideAnim;
			if( vector.x > 0 )
				this.flippedX = false;
			else
				this.flippedX = true;
		}
		
		if( this.currentAnim == animation )
			return;
		
		this.stopWalkAnimation();
		
		this.currentAnim = animation;
		this.runAction( animation );
	},
	
	stopWalkAnimation: function() {
		if( this.currentAnim != null ) {
			this.stopAction( this.currentAnim );
			this.currentAnim = null;
		}
	},

    fire: function(target) {
    	var pos = this.getPosition();
    	SharedData.gameLayer.addChild( new Bullet( cc.p( pos.x, pos.y + 40 ), target ) );
    	
    	this.inCoolDown = true;
    	this.runAction( new cc.Sequence( new cc.DelayTime( SOLDIER_COOLDOWN_TIME ), new cc.CallFunc( this.cooldown, this ) ) );
    },
    
    cooldown: function() {
    	this.inCoolDown = false;
    },
    
    hurt: function(damage) {
    	if( !this.isInvulnerable ) {
    		this.isInvulnerable = true;
	    	this.runAction( new cc.Sequence( new cc.Blink(0.5, 10), new cc.CallFunc( this.invulnerability, this ) ) );
	    	this._super(damage);
    	}
    },
    
    invulnerability: function() {
    	this.isInvulnerable = false;
    },
    
    getUIButtons: function() {
    	return [{text:"Construct Bunker", callback:"startBuild"}];
    }
});