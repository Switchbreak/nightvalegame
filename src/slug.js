SLUG_SPEED	= 100;
SLUG_DAMAGE	= 4;

var Slug = Enemy.extend({
	MaxHP: 300,
	waypoint: null,
	
	walkBackAnim: null,
	walkFrontAnim: null,
	walkSideAnim: null,
	currentAnim: null,
	
	ctor: function() {
		cc.spriteFrameCache.addSpriteFrames(res.CloakedFigure_plist);
		var walkBackFrames = [];
		var walkFrontFrames = [];
		var walkSideFrames = [];
		for( var i = 1; i <= 4; i++ ) {
			walkBackFrames.push( cc.spriteFrameCache.getSpriteFrame("Cloak Black Back " + i + ".png") );
			walkFrontFrames.push( cc.spriteFrameCache.getSpriteFrame("Cloak Black Front " + i + ".png") );
			walkSideFrames.push( cc.spriteFrameCache.getSpriteFrame("Cloak Black Side " + i + ".png") );
		}
		
		this.walkBackAnim = new cc.RepeatForever( new cc.Animate( new cc.Animation( walkBackFrames, 0.2 ) ) );
		this.walkFrontAnim = new cc.RepeatForever( new cc.Animate( new cc.Animation( walkFrontFrames, 0.2 ) ) );
		this.walkSideAnim = new cc.RepeatForever( new cc.Animate( new cc.Animation( walkSideFrames, 0.2 ) ) );
		
		this._super("#Cloak Black Back 1.png");
		this.anchorY = 0.20;
	},
	
	update: function(dt) {
		var collision = this.collisionCheck();
		if( collision != null ) {
			collision.hurt( SLUG_DAMAGE );
		}
		else {
			this.moveTowardGoal(dt);
		}
	},
	
	moveTowardGoal: function(dt) {
		movePos = this.getPosition();
		
		if( this.waypoint != null ) {
			var moveDistance = SLUG_SPEED * dt;
			var vector = cc.p( this.waypoint.x - movePos.x, this.waypoint.y - movePos.y );
			var magnitude = cc.pLength( vector );
			
			if( magnitude <= moveDistance ) {
				movePos = this.waypoint;
				this.waypoint = null;
			}
			else {
				movePos.x += (vector.x / magnitude) * moveDistance;
				movePos.y += (vector.y / magnitude) * moveDistance;
				
				if( cc.pointEqualToPoint( movePos, this.waypoint ) )
					this.waypoint = null;
			}
			
			this.setPosition( movePos );
		}
		else {
			var tilePos = CollisionUtil.getTilePosition(movePos, SharedData.collisionLayer);
			
			var moveTile = this.findMinimumAdjacent(tilePos, SharedData.dijkstraMap);
			if( moveTile == null )
				moveTile = this.findMinimumAdjacent(tilePos, SharedData.blockedDijkstraMap);
			
			if( moveTile != null ) {
				this.waypoint = SharedData.collisionLayer.getPositionAt(moveTile);
				this.waypoint.x += SharedData.collisionLayer.tileWidth / 2;
				this.waypoint.y += SharedData.collisionLayer.tileHeight / 2;
				
				this.playWalkAnimation(cc.p(this.waypoint.x - movePos.x, this.waypoint.y - movePos.y));
			}
		}
	},
	
	playWalkAnimation: function( vector ) {
		var animation;
		
		if( Math.abs( vector.y ) > Math.abs( vector.x ) ) {
			if( vector.y > 0 )
				animation = this.walkBackAnim;
			else
				animation = this.walkFrontAnim;
		}
		else {
			animation = this.walkSideAnim;
			if( vector.x > 0 )
				this.flippedX = false;
			else
				this.flippedX = true;
		}
		
		if( this.currentAnim == animation )
			return;
		
		if( this.currentAnim != null )
			this.stopAction( this.currentAnim );
		
		this.currentAnim = animation;
		this.runAction( animation );
	},
	
	findMinimumAdjacent: function(tilePos, dijkstraMap) {
		var minCost = Number.MAX_VALUE;
		var minTile = null;
		
		var adjacentTiles = [
		                     { row: tilePos.y + 1,	column: tilePos.x },
		                     { row: tilePos.y - 1,	column: tilePos.x },
		                     { row: tilePos.y,		column: tilePos.x + 1 },
		                     { row: tilePos.y,		column: tilePos.x - 1 },
		                     ];
		
		for( var i = 0; i < adjacentTiles.length; i++ ) {
			var cost = this.tileCost(adjacentTiles[i], dijkstraMap);
			if( cost != null && cost < minCost ) {
				minTile = adjacentTiles[i];
				minCost = cost;
			}
		}
		
		if( minTile != null )
			return cc.p(minTile.column, minTile.row);
		
		return null;
	},
	
	tileCost: function( adjacentTile, dijkstraMap ) {
		if( !CollisionUtil.rangeCheck(adjacentTile.row, adjacentTile.column) )
			return null;
		
		if( dijkstraMap[adjacentTile.row][adjacentTile.column] == null )
			return null;
		
		return dijkstraMap[adjacentTile.row][adjacentTile.column].cost;
	},
	
	collisionCheck: function() {
		for( var i = 0; i < SharedData.units.length; i++ ) {
			if( !SharedData.units[i].isStructure && cc.rectIntersectsRect( this.getBoundingBox(), SharedData.units[i].getBoundingBox() ) )
				return SharedData.units[i];
		}
		
		return null;
	}
});