var res = {
    TileSet_png : "res/nightvale.png",
    TileSet_tsx : "res/UndergroundCity.tsx",
    TileMap_tmx : "res/UndergroundCity.tmx",
    Soldier_png : "res/Soldier.png",
    Soldier_plist : "res/Soldier.plist",
    Bullet_png : "res/Bullet.png",
    HealthBar_png : "res/HealthBar.png",
    CloakedFigure_png : "res/CloakedFigure.png",
    CloakedFigure_plist : "res/CloakedFigure.plist",
    Barracks_png : "res/Barracks2.png",
    Crystal_png : "res/Crystals.png",
    ProgressBar_png : "res/ProgressBar.png",
    ChildKing_png : "res/ChildKing.png",
    PillBox_png : "res/PillBox.png",
    SelectionPane_png : "res/SelectionPane.png",
    NightValeIsh_mp3 : "res/nightvaleish.mp3",
    Sandbags_png : "res/Sandbags.png",
    Fireball_png : "res/Fireball.png",
    Explosion_png : "res/Explosion.png",
    Bullet_mp3 : "res/snd/bullet.mp3",
    Hurt_mp3 : "res/snd/hurt.mp3",
    Soldier_mp3 : "res/snd/soldier_alt.mp3",
    TitleScreen_png : "res/TitleScreen.png"
};

var g_resources = [
    //image
    res.Soldier_png,
    res.TileSet_png,
    res.Bullet_png,
    res.HealthBar_png,
    res.CloakedFigure_png,
    res.Barracks_png,
    res.Crystal_png,
    res.ProgressBar_png,
    res.ChildKing_png,
    res.PillBox_png,
    res.SelectionPane_png,
    res.Sandbags_png,
    res.Fireball_png,
    res.Explosion_png,
    res.TitleScreen_png,

    //plist
    res.CloakedFigure_plist,
    res.Soldier_plist,

    //fnt

    //tmx
    res.TileSet_tsx,
    res.TileMap_tmx,

    //bgm
    res.NightValeIsh_mp3

    //effect
];