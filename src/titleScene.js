var TitleLayer = cc.Layer.extend({
	ctor: function() {
		this._super();
		
		var frameSize = cc.director.getWinSize();
		
		var titleScene = new cc.Sprite( res.TitleScreen_png );
		titleScene.setPosition( frameSize.width / 2, frameSize.height / 2 );
		this.addChild( titleScene );
		
		var label = new cc.LabelTTF("Underground City Defender", "Arial", 40);
		label.setPosition( frameSize.width / 2, 3 * frameSize.height / 4 );
		label.lineWidth = 4;
		label.strokeStyle = new cc.Color( 0, 0, 0, 255 );
		this.addChild( label );
		
		var startLabel = new cc.LabelTTF("Click to Start", "Arial", 30);
		startLabel.setPosition( frameSize.width / 2, frameSize.height / 4 );
		startLabel.lineWidth = 2;
		startLabel.strokeStyle = new cc.Color( 0, 0, 0, 255 );
		this.addChild( startLabel );
		
		this.initInput();
	},

	initInput: function () {
		cc.eventManager.addListener({
			event: cc.EventListener.KEYBOARD,
			onKeyPressed: function(keyCode, event) {
				if( keyCode == cc.KEY.space )
					event.getCurrentTarget().restartGame();
			},
		}, this);
		
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseDown: function( event ) {
				event.getCurrentTarget().restartGame();
			},
		}, this)
	},
	
	restartGame: function() {
		cc.director.runScene( cc.TransitionFade.create(1, new NightValeScene()) );
	}
});

var TitleScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new TitleLayer();
        this.addChild(layer);
    }
});