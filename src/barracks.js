var UnitCosts = {
	Soldier: {type: "Soldier", cost: 100},
    Priest: {type: "Priest", cost: 300}
};

var Barracks = Unit.extend({
	MaxHP: 50,
	name: "Barracks",
	inCooldown: false,
	isStructure: true,
	
	ctor: function() {
		this._super(res.Barracks_png);
		
		this.healthBar.visible = false;
	},
	
	update: function() {
		if( this.isSelected ) {
			if( SharedData.keyboard[cc.KEY["1"]] )
				this.buyUnit( UnitCosts.Soldier );
			if( SharedData.keyboard[cc.KEY["2"]] )
				this.buyUnit( UnitCosts.Priest );
		}
	},
    
    hurt: function(damage) {},
    
    buyUnit: function( unitCost ) {
    	if( this.inCooldown )
    		return;
    	
		cc.audioEngine.playEffect( res.Soldier_mp3 );
		
    	if( SharedData.resources >= unitCost.cost ) {
			var entity = SharedData.gameLayer.spawnGameObject(unitCost.type, this.x, this.y - 40);
			SharedData.gameLayer.selectUnit(entity);
			
			SharedData.resources -= unitCost.cost;
			SharedData.gameLayer.updateResources();
    	}
    	
		this.inCooldown = true;
		this.runAction( new cc.Sequence( new cc.DelayTime( 0.5 ), new cc.CallFunc( this.cooldown, this ) ) );
    },
    
    buySoldier: function() {
        this.buyUnit( UnitCosts.Soldier );
    },
    
    buyPriest: function() {
        this.buyUnit( UnitCosts.Priest );
    },
    
    cooldown: function() {
    	this.inCooldown = false;
    },
    
    getUIButtons: function() {
    	return [{text:"Build Soldier - 100ru", callback:"buySoldier"},
        {text:"Build Priest - 300ru", callback:"buyPriest"}];
    }
});