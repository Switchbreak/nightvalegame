var gameObjectTable = {
	Soldier:	{ type: Soldier,	enemy: false, 	unit: true, 	goal: false, 	structure: false, 	resource: false,	waveActivate: false },
	Priest:		{ type: Priest,		enemy: false, 	unit: true, 	goal: false, 	structure: false, 	resource: false,	waveActivate: false },
	King: 		{ type: King, 		enemy: false, 	unit: true, 	goal: true, 	structure: false, 	resource: false,	waveActivate: false },
	Slug: 		{ type: Slug, 		enemy: true, 	unit: false, 	goal: false, 	structure: false, 	resource: false,	waveActivate: false },
	SlugSpawn: 	{ type: SlugSpawn, 	enemy: false, 	unit: false, 	goal: false, 	structure: false, 	resource: false,	waveActivate: true },
	Barracks: 	{ type: Barracks, 	enemy: false, 	unit: true, 	goal: false, 	structure: true, 	resource: false,	waveActivate: false },
	Bunker: 	{ type: Bunker, 	enemy: false, 	unit: true, 	goal: false, 	structure: true, 	resource: false,	waveActivate: false },
	FireBunker: { type: FireBunker, enemy: false, 	unit: true, 	goal: false, 	structure: true, 	resource: false,	waveActivate: false },
	Crystal: 	{ type: Crystal, 	enemy: false, 	unit: false, 	goal: false, 	structure: false, 	resource: true,		waveActivate: false }
};