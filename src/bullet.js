BULLET_SPEED	= 1000;
BULLET_LIFE		= 0.3;
BULLET_DAMAGE	= 4;

var Bullet = cc.Sprite.extend({
	bulletVector: null,
	
	ctor: function( position, target ) {
		this._super( res.Bullet_png );
		this.setAnchorPoint( 0.5, 0.5 );
		
		this.bulletVector = cc.pNormalize( new cc.Point( target.x - position.x, target.y - position.y ) );
		
		this.setPosition( position );
		this.runAction( new cc.Sequence( new cc.DelayTime(BULLET_LIFE), new cc.CallFunc( this.removeSelf, this ) ) );
		this.scheduleUpdate();
		
		cc.audioEngine.playEffect( res.Bullet_mp3 );
	},
	
	update: function( dt ) {
		var position = this.getPosition();
		var newPosition = new cc.Point( position.x + this.bulletVector.x * BULLET_SPEED * dt, position.y + this.bulletVector.y * BULLET_SPEED * dt );
		
		this.movePosition( newPosition );
	},
	
	movePosition: function( newPosition ) {
		this.setPosition( newPosition );
		
		var collision = this.enemyCollisionCheck( newPosition );
		if( collision != null ) {
			collision.hurt( BULLET_DAMAGE );
			this.removeSelf();
			return;
		}
		
		if( this.collisionCheck( newPosition ) ) {
			this.removeSelf();
			return;
		}
	},
	
	enemyCollisionCheck: function( newPosition ) {
		for( var i = 0; i < SharedData.enemies.length; i++ ) {
			var boundingBox = SharedData.enemies[i].getBoundingBox();
			if( cc.rectIntersectsRect( boundingBox, this.getBoundingBox() ) ) {
				return SharedData.enemies[i];
			}
		}
		
		return null;
	},
	
	collisionCheck: function( newPosition ) {
		var collisionLayer = SharedData.collisionLayer;
		var tilePos = CollisionUtil.getTilePosition( newPosition, collisionLayer );
		
		if( CollisionUtil.isTileSolid(tilePos, collisionLayer) )
			return true;
		
		return false;
	},

	removeSelf: function() {
		this.parent.removeChild( this );
	}
});