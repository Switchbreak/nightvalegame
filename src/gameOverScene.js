var GameOverLayer = cc.Layer.extend({
	ctor: function() {
		this._super();
		
		var frameSize = cc.director.getWinSize();
		
		var label = new cc.LabelTTF("Game Over", "Arial", 40);
		label.setPosition( frameSize.width / 2, frameSize.height / 2 );
		this.addChild( label );
		
		this.initInput();
	},

	initInput: function () {
		cc.eventManager.addListener({
			event: cc.EventListener.KEYBOARD,
			onKeyPressed: function(keyCode, event) {
				if( keyCode == cc.KEY.space )
					event.getCurrentTarget().restartGame();
			},
		}, this);
		
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseDown: function( event ) {
				event.getCurrentTarget().restartGame();
			},
		}, this)
	},
	
	restartGame: function() {
		cc.director.runScene( cc.TransitionFade.create(1, new NightValeScene()) );
	}
});

var GameOverScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new GameOverLayer();
        this.addChild(layer);
    }
});