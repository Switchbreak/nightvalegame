var CollisionUtil = {
    getTilePosition: function(pos, tileLayer) {
    	var tilePosition = new cc.Point(0, 0);
    	tilePosition.x = Math.floor( pos.x / tileLayer.tileWidth );
    	tilePosition.y = Math.floor( (tileLayer.height - pos.y) / tileLayer.tileHeight );
    	
    	if( tilePosition.y >= tileLayer.layerHeight )
    	    tilePosition.y = tileLayer.layerHeight - 1;
    	
    	return tilePosition;
    },
    
    isTileSolid: function(tilePos, tileLayer, includeStructures) {
    	if( tilePos.x < 0 || tilePos.y < 0 ||
    		tilePos.x >= tileLayer.layerWidth ||
    		tilePos.y >= tileLayer.layerHeight ) {
    	    return false;
    	}

    	var tileGID = tileLayer.getTileGIDAt( tilePos );
    	var properties = SharedData.tileMap.propertiesForGID( tileGID );
	    if( properties == null || properties["Passable"] != "True" ) {
	    	return true;
	    }
	    
	    if( includeStructures ) {
	    	for( var i = 0; i < SharedData.structures.length; i++ ) {
	    		var structurePos = this.getTilePosition(SharedData.structures[i].getPosition(), tileLayer);
	    		if( cc.pointEqualToPoint( tilePos, structurePos ) )
	    			return true;
	    	}
	    }
	    
	    return false;
    },
    
    getDijkstraMap: function ( includeStructures ) {
        var tiles = [];
        var currentTiles = new BinaryHeap( function(a, b) { return a.cost - b.cost; } );
        var visitedTiles = [];
        var collisionLayer = SharedData.collisionLayer;
        
        for( var row = 0; row < collisionLayer.layerHeight; row++ ) {
            tiles[row] = [];
        }
        
        var goalPosition = this.getTilePosition(SharedData.goalUnit.getPosition(), collisionLayer);
        var currentTile = { cost: 0, previous: null, row: goalPosition.y, column: goalPosition.x };
        currentTiles.add( currentTile );
        tiles[goalPosition.y][goalPosition.x] = currentTile;
        
        while( currentTiles.length() > 0 ) {
            var currentTile = currentTiles.remove();
            
            var adjacentTiles = this.getAdjacentMoves( currentTile.row, currentTile.column, includeStructures );
            for( var i = 0; i < adjacentTiles.length; i++ ) {
                var adjacentCost = currentTile.cost + adjacentTiles[i].cost;
                
                if( tiles[adjacentTiles[i].row][adjacentTiles[i].column] == null ) {
                    var newTile = { cost: adjacentCost, previous: currentTile, row: adjacentTiles[i].row, column: adjacentTiles[i].column };
                    currentTiles.add( newTile );
                    tiles[adjacentTiles[i].row][adjacentTiles[i].column] = newTile;
                }
                else if( tiles[adjacentTiles[i].row][adjacentTiles[i].column].cost > adjacentCost ) {
                    tiles[adjacentTiles[i].row][adjacentTiles[i].column].cost = adjacentCost;
                    tiles[adjacentTiles[i].row][adjacentTiles[i].column].previous = currentTile;
                }
            }
        }
        
        return tiles;
    },
    
    getAdjacentMoves: function (row, column, includeStructures) {
        adjacentMoves = [];
        
        this.checkAdjacentMove( row + 1,  column,     1,    adjacentMoves, includeStructures );
        this.checkAdjacentMove( row,      column + 1, 1,    adjacentMoves, includeStructures );
        this.checkAdjacentMove( row,      column - 1, 1,    adjacentMoves, includeStructures );
        this.checkAdjacentMove( row - 1,  column,     1,    adjacentMoves, includeStructures );
        
        return adjacentMoves;
    },
    
    checkAdjacentMove: function (row, column, moveCost, adjacentMoves, includeStructures) {
        if( !this.rangeCheck( row, column ) )
            return;
        
        if( this.isTileSolid(cc.p(column, row), SharedData.collisionLayer, includeStructures) )
            return;
        
        adjacentMoves.push( {row: row, column: column, cost: moveCost} );
    },
    
    rangeCheck: function (row, column) {
        if( row < 0 || row >= SharedData.collisionLayer.layerHeight )
            return false;
        if( column < 0 || column >= SharedData.collisionLayer.layerWidth )
            return false;
        
        return true;
    },
};