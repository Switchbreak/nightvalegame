var Enemy = cc.Sprite.extend({
	HP: 0,
	MaxHP: 0,
	healthBar: null,
	
	ctor: function( fileName ) {
		this._super( fileName );
		this.scheduleUpdate();
		
		this.HP = this.MaxHP;
		
		this.healthBar = cc.ProgressTimer.create(new cc.Sprite( res.HealthBar_png ));
		this.healthBar.type = cc.ProgressTimer.TYPE_BAR;
		this.healthBar.x = this.width / 2;
		this.healthBar.y = this.height + 10;
		this.healthBar.midPoint = cc.p( 0, 0 );
		this.healthBar.barChangeRate = cc.p( 1, 0 );
		this.healthBar.percentage = 100;
		this.addChild( this.healthBar );
	},
	
	hurt: function( damage ) {
		this.HP -= damage;
		this.healthBar.percentage = (this.HP / this.MaxHP) * 100;
		
		if( this.HP <= 0 )
			this.die();
	},
	
	die: function() {
		SharedData.enemies.splice(SharedData.enemies.indexOf( this ), 1)
		this.parent.removeChild( this );
	}
});