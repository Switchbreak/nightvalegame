var SharedData = {
	keyboard: [],
	isLeftMouseDown: false,
	isRightMouseDown: false,
	mouseLocation: null,
	gameLayer: null,
	tileMap: null,
	collisionLayer: null,
	units: [],
	enemies: [],
	resourceObjects: [],
	structures: [],
	goalUnit: null,
	selectedUnit: null,
	dijkstraMap: null,
	blockedDijkstraMap: null,
	resources: 20,
	waveTime: 30,
	currentWave: 0
};

SCROLL_SPEED = 300;

var UILayer;

var NightValeLayer = cc.Layer.extend({
	waveActive: 0,
	
	followAction: null,
	selectedUnitLabel: null,
	resourcesLabel: null,
	waveTimeLabel: null,
	UIButtonMenu: null,
	
    ctor:function () {
        this._super();
        SharedData.gameLayer = this;
        
        this.initSharedData();
        this.initUI();
        this.initTileMap();
        this.initInput();
        
        this.updateResources();
        this.updateWaveTime();
        
        SharedData.dijkstraMap = CollisionUtil.getDijkstraMap(true);
        SharedData.blockedDijkstraMap = CollisionUtil.getDijkstraMap(false);
        
        this.scheduleUpdate();
        this.startWaveTimer();
        
        return true;
    },
    
    onEnter: function() {
    	this._super();
        cc.audioEngine.playMusic(res.NightValeIsh_mp3, true);
    },
    
    onExit: function() {
    	this._super();
        cc.audioEngine.stopMusic(true);
    },
    
    toggleMusic: function() {
    	if( cc.audioEngine.isMusicPlaying() ) {
    		cc.audioEngine.pauseMusic();
    	}
    	else {
    		cc.audioEngine.resumeMusic();
    	}
    },
    
    update: function( dt ) {
		if( SharedData.keyboard[cc.KEY.q] )
			this.endGame();
		if( SharedData.keyboard[cc.KEY.b] )
			this.selectBarracks();
		
		if( SharedData.selectedUnit == null || SharedData.selectedUnit.isStructure ) {
			this.scrollingInput(dt);
		}
    },
    
    scrollingInput: function( dt ) {
		if( SharedData.keyboard[cc.KEY.w] || SharedData.keyboard[cc.KEY.up] ) {
			this.y -= SCROLL_SPEED * dt;
			if( this.y < -SharedData.tileMap.height + this.height )
				this.y = -SharedData.tileMap.height + this.height;
		}
		if( SharedData.keyboard[cc.KEY.s] || SharedData.keyboard[cc.KEY.down] ) {
			this.y += SCROLL_SPEED * dt;
			if( this.y > 0 )
				this.y = 0;
		}
		if( SharedData.keyboard[cc.KEY.a] || SharedData.keyboard[cc.KEY.left] ) {
			this.x += SCROLL_SPEED * dt;
			if( this.x > 0 )
				this.x = 0;
		}
		if( SharedData.keyboard[cc.KEY.d] || SharedData.keyboard[cc.KEY.right] ) {
			this.x -= SCROLL_SPEED * dt;
			if( this.x < -SharedData.tileMap.width + this.width )
				this.x = -SharedData.tileMap.width + this.width;
		}
    },
    
    initUI: function () {
    	var frameSize = cc.director.getWinSize();
    	
    	var selectionPane = new cc.Sprite( res.SelectionPane_png );
    	selectionPane.setAnchorPoint( 0, 0 );
    	UILayer.addChild( selectionPane );
    	
        this.selectedUnitLabel = new cc.LabelTTF( "", "Arial", 32 );
        this.selectedUnitLabel.setAnchorPoint( 0, 0 );
        this.selectedUnitLabel.setPosition( 10, 10 );
        this.selectedUnitLabel.visible = false;
        UILayer.addChild( this.selectedUnitLabel );
        
        this.UIButtonMenu = new cc.Menu();
        this.UIButtonMenu.setPosition( frameSize.width / 2, 32 );
        this.UIButtonMenu.visible = false;
        UILayer.addChild( this.UIButtonMenu );
        
        var crystalIcon = new cc.Sprite( res.Crystal_png );
        crystalIcon.setAnchorPoint( 0, 1 );
        crystalIcon.setPosition( 10, frameSize.height );
        crystalIcon.scale = 0.5;
        UILayer.addChild( crystalIcon );
        
        this.resourcesLabel = new cc.LabelTTF( "", "Arial", 26 );
        this.resourcesLabel.setAnchorPoint( 0, 1 );
        this.resourcesLabel.setPosition( 50, frameSize.height - 5 );
        UILayer.addChild( this.resourcesLabel );
        
        this.waveTimeLabel = new cc.LabelTTF( "", "Arial", 26 );
        this.waveTimeLabel.setAnchorPoint( 0.5, 1 );
        this.waveTimeLabel.setPosition( frameSize.width / 2, frameSize.height - 5 );
        UILayer.addChild( this.waveTimeLabel );
    },
    
    initTileMap: function () {
        var tileMap = cc.TMXTiledMap.create(res.TileMap_tmx);
        this.addChild( tileMap );
        SharedData.tileMap = tileMap;
        SharedData.collisionLayer = tileMap.getLayer("Bottom");
        
        this.initMapObjects();
    },
    
    initMapObjects: function () {
    	var gameObjects = SharedData.tileMap.getObjectGroup("GameObjects").getObjects();
    	
    	for( var i = 0; i < gameObjects.length; i++ ) {
    		var objectInfo = gameObjects[i];
    		var entity = this.spawnGameObject( objectInfo.type, objectInfo.x, objectInfo.y );
			if( entity != null && objectInfo.DefaultUnit == "true" )
				this.selectUnit(entity);
    	}
    },

	initInput: function () {
		cc.eventManager.addListener({
			event: cc.EventListener.KEYBOARD,
			onKeyPressed: function(keyCode, event) { SharedData.keyboard[keyCode] = true; },
			onKeyReleased: function(keyCode, event) {
				SharedData.keyboard[keyCode] = false;
				
				if( keyCode == cc.KEY.n )
					event.getCurrentTarget().selectNextUnit();
				if( keyCode == cc.KEY.m )
					event.getCurrentTarget().toggleMusic();
			}
		}, this);
		
		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseMove: function( event ) {
				SharedData.mouseLocation = event.getLocation();
			},
			onMouseDown: function( event ) {
				if( event.getButton() == cc.EventMouse.BUTTON_LEFT ) {
					SharedData.isLeftMouseDown = true;
					event.getCurrentTarget().detectUnitClick();
				}
				else if( event.getButton() == cc.EventMouse.BUTTON_RIGHT )
					SharedData.isRightMouseDown = true;
			},
			onMouseUp: function( event ) {
				if( event.getButton() == cc.EventMouse.BUTTON_LEFT )
					SharedData.isLeftMouseDown = false;
				else if( event.getButton() == cc.EventMouse.BUTTON_RIGHT )
					SharedData.isRightMouseDown = false;
			}
		}, this);
	},
	
	initSharedData: function () {
		SharedData.isLeftMouseDown = false;
		SharedData.isRightMouseDown = false;
		SharedData.keyboard = [];
		SharedData.units = [];
		SharedData.resourceObjects = [];
		SharedData.enemies = [];
		SharedData.structures = [];
		SharedData.resources = 50;
		SharedData.waveTime = 30;
		SharedData.currentWave = 0;
	},
	
	updateResources: function () {
		this.resourcesLabel.string = SharedData.resources;
	},
	
	updateWaveTime: function () {
		if( SharedData.waveTime > 0 ) {
			var minutes = Math.floor( SharedData.waveTime / 60 );
			var seconds = Math.floor( SharedData.waveTime ) - minutes * 60;
			if( seconds < 10 )
				seconds = "0" + seconds;
			
			this.waveTimeLabel.string = "Next Wave: " + minutes + ":" + seconds;
		}
		else {
			this.waveTimeLabel.string = "Wave " + SharedData.currentWave;
		}
	},
	
	startWaveTimer: function() {
		SharedData.currentWave++;
		SharedData.waveTime = 30;
		this.runAction( new cc.Sequence( new cc.Repeat( new cc.Sequence( new cc.DelayTime( 1 ), new cc.CallFunc( this.tickWaveTimer, this ) ), SharedData.waveTime + 1 ), new cc.CallFunc( this.nextWave, this ) ) );
	},
	
	tickWaveTimer: function() {
		SharedData.waveTime--;
		this.updateWaveTime();
	},
	
	nextWave: function () {
		this.runAction( new cc.Sequence( new cc.DelayTime( 30 ), new cc.CallFunc( this.startWaveTimer, this ) ) );
	},
	
	spawnGameObject: function( name, x, y ) {
		var gameObject = gameObjectTable[name];
		
		if( gameObject ) {
    		var entity = new gameObject.type();
    		entity.setPosition( x, y );
    		
    		this.addChild( entity );
    		
    		if( gameObject.waveActivate )
    			entity.waveActive = ++this.waveActive;
    		
    		if( gameObject.enemy ) {
    			SharedData.enemies.push( entity );
    		}
    		else if( gameObject.unit ) {
    			SharedData.units.push( entity );
    			
    			if( gameObject.goal )
    				SharedData.goalUnit = entity;
    			if( gameObject.structure )
    				SharedData.structures.push( entity );
    		}
    		else if( gameObject.resource )
    			SharedData.resourceObjects.push( entity );
    		
    		return entity;
		}
	},
	
	detectUnitClick: function() {
		var clickPosition = this.convertToNodeSpace( SharedData.mouseLocation );
		for( var i = 0; i < SharedData.units.length; i++ ) {
			if( cc.rectContainsPoint( SharedData.units[i].getBoundingBox(), clickPosition ) ) {
				this.selectUnit( SharedData.units[i] );
				return;
			}
		}
	},
	
	selectUnit: function( unit ) {
		this.deselectUnit();
		
		SharedData.selectedUnit = unit;
		unit.select();
		
		if( !unit.isStructure ) {
			this.followAction = new cc.Follow( unit, cc.rect( 0, 0, SharedData.tileMap.width, SharedData.tileMap.height ) );
	        this.runAction( this.followAction );
		}
        
		this.showUnitUI( unit );
	},
	
	showUnitUI: function( unit ) {
        this.selectedUnitLabel.string = unit.name;
        this.selectedUnitLabel.visible = true;
        
        this.UIButtonMenu.visible = true;
        this.UIButtonMenu.removeAllChildren();
        var UIButtons = unit.getUIButtons();
        for( var i = 0; i < UIButtons.length; i++ ) {
        	var newButton = new cc.MenuItemFont(UIButtons[i].text,UIButtons[i].callback,unit);
         newButton.setFontSize(16);
        	this.UIButtonMenu.addChild( newButton );
        }
        this.UIButtonMenu.alignItemsVertically();
	},
	
	deselectUnit: function() {
		if( SharedData.selectedUnit != null )
			SharedData.selectedUnit.deselect();
		if( this.followAction != null )
			this.stopAction( this.followAction );
		
		SharedData.selectedUnit = null;
		this.followAction = null;
		this.selectedUnitLabel.visible = false;
	},
	
	selectNextUnit: function() {
		var unitIndex = -1;
		if( SharedData.selectedUnit != null ) {
			unitIndex = SharedData.units.indexOf( SharedData.selectedUnit );
		}
		
		unitIndex = (unitIndex + 1) % SharedData.units.length;
		this.selectUnit(SharedData.units[unitIndex]);
	},
	
	selectBarracks: function() {
		for( var i = 0; i < SharedData.units.length; i++ ) {
			if( SharedData.units[i].name == "Barracks" ) {
				this.selectUnit(SharedData.units[i]);
				return;
			}
		}
	},
	
	removeUnit: function( unit ) {
		this.removeChild( unit );
		SharedData.units.splice( SharedData.units.indexOf( unit ), 1 );
		
		if( unit.isStructure ) {
			SharedData.structures.splice( SharedData.structures.indexOf( unit ), 1 );
			SharedData.dijkstraMap = CollisionUtil.getDijkstraMap(true);
		}
		
		if( unit == SharedData.selectedUnit ) {
			this.deselectUnit();
			
			if( SharedData.units.length > 0 )
				this.selectUnit( SharedData.units[0] );
		}
		
		if( SharedData.goalUnit == unit ) {
			SharedData.goalUnit = null;
			this.endGame();
		}
	},
	
	endGame: function() {
		this.zoomOut();
		cc.director.runScene( cc.TransitionFade.create(1, new GameOverScene()) );
	},
	
	zoomOut: function() {
		this.anchorX = this.x / this.width + 0.5;
		this.anchorY = -this.y / this.height + 0.5;
		this.runAction( cc.ScaleTo.create(1, 0.01, 0.01) );
	}
});

var NightValeScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        
        UILayer = new cc.Layer();
        var layer = new NightValeLayer();
        this.addChild(layer);
        this.addChild(UILayer);
    }
});