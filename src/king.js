var King = Unit.extend({
	MaxHP: 50,
	isInvulnerable: false,
	name: "The Child-King",
	
	ctor: function() {
		this._super(res.ChildKing_png);
	},
	
	update: function() {},
    
    hurt: function(damage) {
    	if( !this.isInvulnerable ) {
    		this.isInvulnerable = true;
	    	this.runAction( new cc.Sequence( new cc.Blink(0.5, 10), new cc.CallFunc( this.invulnerability, this ) ) );
	    	this._super(damage);
    	}
    },
    
    invulnerability: function() {
    	this.isInvulnerable = false;
    }
});