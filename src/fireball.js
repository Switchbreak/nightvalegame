FIREBALL_SPEED		= 700;
FIREBALL_LIFE		= 0.3;
FIREBALL_DAMAGE		= 10;
EXPLODE_RADIUS_SQ 	= 10000;

var Fireball = cc.Sprite.extend({
	bulletVector: null,
	
	ctor: function( position, target ) {
		this._super( res.Fireball_png );
		this.setAnchorPoint( 0.5, 0.5 );
		
		this.bulletVector = cc.pNormalize( new cc.Point( target.x - position.x, target.y - position.y ) );
		
		this.setPosition( position );
		this.runAction( new cc.Sequence( new cc.DelayTime(FIREBALL_LIFE), new cc.CallFunc( this.explode, this ) ) );
		this.scheduleUpdate();
		
		cc.audioEngine.playEffect( res.Bullet_mp3 );
	},
	
	update: function( dt ) {
		var position = this.getPosition();
		var newPosition = new cc.Point( position.x + this.bulletVector.x * FIREBALL_SPEED * dt, position.y + this.bulletVector.y * FIREBALL_SPEED * dt );
		
		this.movePosition( newPosition );
	},
	
	movePosition: function( newPosition ) {
		this.setPosition( newPosition );
		
		var collision = this.enemyCollisionCheck( newPosition );
		if( collision != null ) {
			this.explode();
			return;
		}
		
		if( this.collisionCheck( newPosition ) ) {
			this.explode();
			return;
		}
	},
	
	enemyCollisionCheck: function( newPosition ) {
		for( var i = 0; i < SharedData.enemies.length; i++ ) {
			var boundingBox = SharedData.enemies[i].getBoundingBox();
			if( cc.rectIntersectsRect( boundingBox, this.getBoundingBox() ) ) {
				return SharedData.enemies[i];
			}
		}
		
		return null;
	},
	
	collisionCheck: function( newPosition ) {
		var collisionLayer = SharedData.collisionLayer;
		var tilePos = CollisionUtil.getTilePosition( newPosition, collisionLayer );
		
		if( CollisionUtil.isTileSolid(tilePos, collisionLayer) )
			return true;
		
		return false;
	},

    explode: function() {
    	this.addChild( new cc.Sprite( res.Explosion_png ) );
    	
        var pos = this.getPosition();
        for( var i = 0; i < SharedData.enemies.length; i++ ) {
            if( cc.pDistanceSQ( pos, SharedData.enemies[i].getPosition() ) < EXPLODE_RADIUS_SQ ) {
                SharedData.enemies[i].hurt( FIREBALL_DAMAGE );
            }
        }
        
        this.unscheduleUpdate();
        this.runAction( new cc.Sequence( new cc.DelayTime( 0.05 ), new cc.CallFunc( this.removeSelf, this ) ) );
    },

	removeSelf: function() {
		this.parent.removeChild( this );
	}
});