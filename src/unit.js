var Unit = cc.Sprite.extend({
	HP: 0,
	MaxHP: 0,
	isSelected: false,
	waypoint: null,
	name: "Unit",
	isStructure: false,
	harvesting: null,
	
	ctor: function( fileName ) {
		this._super( fileName );
		this.scheduleUpdate();
		
		this.HP = this.MaxHP;
		
		this.healthBar = cc.ProgressTimer.create(new cc.Sprite( res.HealthBar_png ));
		this.healthBar.type = cc.ProgressTimer.TYPE_BAR;
		this.healthBar.x = this.width / 2;
		this.healthBar.y = this.height + 10;
		this.healthBar.midPoint = cc.p( 0, 0 );
		this.healthBar.barChangeRate = cc.p( 1, 0 );
		this.healthBar.percentage = 100;
		this.addChild( this.healthBar );
	},
	
	update: function ( dt ) {
		if( this.isSelected ) {
			var newPos = this.movementInput(dt);
			this.rangeCheck(newPos);
			while( this.collisionDetect( this.getPosition(), newPos ) );
			
			var pos = this.getPosition();
			if( !cc.pointEqualToPoint( pos, newPos ) )
				this.playWalkAnimation(cc.p(newPos.x - pos.x, newPos.y - pos.y));
			else
				this.stopWalkAnimation();
			
			this.setPosition( newPos );
		}
		
		this.resourceCollisionCheck(dt);
	},
	
	movementInput: function ( dt ) {
		var movePos = this.getPosition();
		
		if( SharedData.isRightMouseDown ) {
			this.waypoint = SharedData.gameLayer.convertToNodeSpace( SharedData.mouseLocation );
		}
		
		if( SharedData.keyboard[cc.KEY.w] || SharedData.keyboard[cc.KEY.up] ) {
			movePos.y += MOVE_SPEED * dt;
			this.waypoint = null;
		}
		if( SharedData.keyboard[cc.KEY.s] || SharedData.keyboard[cc.KEY.down] ) {
			movePos.y -= MOVE_SPEED * dt;
			this.waypoint = null;
		}
		if( SharedData.keyboard[cc.KEY.a] || SharedData.keyboard[cc.KEY.left] ) {
			movePos.x -= MOVE_SPEED * dt;
			this.waypoint = null;
		}
		if( SharedData.keyboard[cc.KEY.d] || SharedData.keyboard[cc.KEY.right] ) {
			movePos.x += MOVE_SPEED * dt;
			this.waypoint = null;
		}
		
		if( this.waypoint != null ) {
			var moveDistance = MOVE_SPEED * dt;
			var vector = cc.p( this.waypoint.x - movePos.x, this.waypoint.y - movePos.y );
			var magnitude = cc.pLength( vector );
			
			if( magnitude < moveDistance ) {
				movePos = this.waypoint;
				this.waypoint = null;
			}
			else {
				movePos.x += (vector.x / magnitude) * moveDistance;
				movePos.y += (vector.y / magnitude) * moveDistance;
			}
		}
		
		return movePos;
	},
	
	playWalkAnimation: function(vector) {},
	
	stopWalkAnimation: function(vector) {},
	
	rangeCheck: function( pos ) {
		if( pos.x < 0 )
			pos.x = 0;
		if( pos.y < 0 )
			pos.y = 0;
		if( pos.x > SharedData.collisionLayer.width )
			pos.x = SharedData.collisionLayer.width;
		if( pos.y > SharedData.collisionLayer.height )
			pos.y = SharedData.collisionLayer.height;
	},
	
    collisionDetect: function(startingPos, pos) {
        var vector = new cc.Point( pos.x - startingPos.x, pos.y - startingPos.y );
		var collisionLayer = SharedData.collisionLayer;
		var sweepTilePos = CollisionUtil.getTilePosition( startingPos, collisionLayer );
		var endTilePos = CollisionUtil.getTilePosition( pos, collisionLayer );
		
		while( !cc.pointEqualToPoint( sweepTilePos, endTilePos ) ) {
		    var sweepPos = collisionLayer.getPositionAt( sweepTilePos );
		    
		    if( sweepTilePos.x !== endTilePos.x ) {
				if( pos.x < startingPos.x ) {
				    // Check for intersection on left side
				    var intersection = pos.y + vector.y / vector.x * (sweepPos.x - pos.x) - sweepPos.y;
				    if( intersection <= collisionLayer.tileHeight ) {
						sweepTilePos.x--;
			
						if( CollisionUtil.isTileSolid( sweepTilePos, collisionLayer ) ) {
						    pos.x = sweepPos.x + EPSILON;
						    return true;
						}
				    }
				}
				else if( pos.x > startingPos.x ) {
				    // Check for intersection on the right side
				    var intersection = pos.y + vector.y / vector.x * (sweepPos.x + collisionLayer.tileWidth - pos.x) - sweepPos.y;
				    if( intersection <= collisionLayer.tileHeight ) {
						sweepTilePos.x++;
			
						if( CollisionUtil.isTileSolid( sweepTilePos, collisionLayer ) ) {
						    pos.x = sweepPos.x + collisionLayer.tileWidth - EPSILON;
						    return true;
						}
				    }
				}
		    }
		    
		    if( sweepTilePos.y !== endTilePos.y ) {
				if( pos.y > startingPos.y ) {
				    // Check for intersection on top side
				    var intersection = pos.x + vector.x / vector.y * (sweepPos.y + collisionLayer.tileHeight - pos.y) - sweepPos.x;
				    if( intersection <= collisionLayer.tileWidth ) {
						sweepTilePos.y--;
			
						if( CollisionUtil.isTileSolid( sweepTilePos, collisionLayer ) ) {
						    pos.y = sweepPos.y + collisionLayer.tileHeight - EPSILON;
						    return true;
						}
				    }
				}
				else if( pos.y < startingPos.y ) {
				    // Check for intersection on bottom side
				    var intersection = pos.x + vector.x / vector.y * (sweepPos.y - pos.y) - sweepPos.x;
				    if( intersection <= collisionLayer.tileWidth ) {
						sweepTilePos.y++;
			
						if( CollisionUtil.isTileSolid( sweepTilePos, collisionLayer ) ) {
						    pos.y = sweepPos.y + EPSILON;
						    return true;
						}
				    }
				}
		    }
		}
		
		return false;
    },
    
    resourceCollisionCheck: function (dt) {
    	var boundingBox = this.getBoundingBox();
    	for( var i = 0; i < SharedData.resourceObjects.length; i++ ) {
    		if( cc.rectIntersectsRect( boundingBox, SharedData.resourceObjects[i].getBoundingBox() ) ) {
    			SharedData.resourceObjects[i].harvest(dt);
    			this.harvesting = SharedData.resourceObjects[i];
    			return;
    		}
    	}
    	
    	if( this.harvesting != null ) {
    		this.harvesting.stopHarvest();
    		this.harvesting = null;
    	}
    },
    
    select: function() {
    	this.isSelected = true;
    },
    
    
    deselect: function() {
    	this.isSelected = false;
    },
    

    hurt: function(damage) {
		cc.audioEngine.playEffect( res.Hurt_mp3 );
		
    	this.HP -= damage;
		this.healthBar.percentage = (this.HP / this.MaxHP) * 100;
		
    	if( this.HP <= 0 )
    		this.die();
    },
    
    
    die: function() {
    	SharedData.gameLayer.removeUnit(this);
    	
    	if( this.isStructure )
			SharedData.enemies.splice(SharedData.enemies.indexOf( this ), 1)
    },
    
    getUIButtons: function() {
    	return [];
    }
});