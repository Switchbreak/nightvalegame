SPAWN_RATE = 5;

var SlugSpawn = cc.Node.extend({
	waveActive: 0,
	
	ctor: function() {
		this._super();
		
		this.randomSpawnDelay();
	},
	
	randomSpawnDelay: function() {
		var spawnRate = SPAWN_RATE + cc.randomMinus1To1();
		this.runAction( new cc.Sequence( new cc.DelayTime( spawnRate ), new cc.CallFunc( this.spawnSlug, this ) ) );
	},
	
	spawnSlug: function() {
		if( SharedData.waveTime <= 0 && SharedData.currentWave >= this.waveActive ) {
			SharedData.gameLayer.spawnGameObject("Slug", this.x, this.y);
		}
		this.randomSpawnDelay();
	}
});