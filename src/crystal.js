RESOURCE_VALUE	= 50;
MINING_TIME		= 3;

var Crystal = cc.Sprite.extend({
	progressBar: null,
	miningTime: MINING_TIME,
	
	ctor: function() {
		this._super(res.Crystal_png);
		
		this.progressBar = cc.ProgressTimer.create(new cc.Sprite(res.ProgressBar_png));
		this.progressBar.type = cc.ProgressTimer.TYPE_RADIAL;
		this.progressBar.percentage = 0;
		this.progressBar.setPosition( this.x + this.width / 2, this.y + this.height / 2 );
		this.addChild(this.progressBar);
	},
	
	harvest: function(dt) {
		if( this.miningTime <= 0 ) {
			SharedData.resources += RESOURCE_VALUE;
			SharedData.gameLayer.updateResources();
			
			this.parent.removeChild( this );
			SharedData.resourceObjects.splice( SharedData.resourceObjects.indexOf( this ), 1 );
		}
		else {
			this.progressBar.percentage = 100 * (MINING_TIME - this.miningTime) / MINING_TIME;
			
			this.miningTime -= dt;
		}
	},
	
	stopHarvest: function() {
		this.miningTime = MINING_TIME;
		this.progressBar.percentage = 0;
	}
});