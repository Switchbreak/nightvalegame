var Bunker = Unit.extend({
	MaxHP: 50,
	name: "Bunker",
	inCooldown: false,
	isStructure: true,
	
	ctor: function() {
		this._super(res.Sandbags_png);
		
		this.healthBar.visible = false;
	},
	
	update: function() {
		if( this.isSelected ) {
			if( SharedData.keyboard[cc.KEY.k] )
				this.unbuild();
		}
		this.unitAI();
	},
	
	unitAI: function() {
		if( !this.inCoolDown ) {
			var target = this.pickEnemyTarget();
			if( target != null )
				this.fire( target.getPosition() );
		}
	},
	
	pickEnemyTarget: function() {
		var minDist = Number.MAX_VALUE;
		var minTarget = null;
		
		for( var i = 0; i < SharedData.enemies.length; i++ ) {
			var distance = cc.pDistanceSQ( this.getPosition(), SharedData.enemies[i].getPosition() );
			
			if( distance < AI_FIRE_RANGE_SQ && distance < minDist ) {
				minTarget = SharedData.enemies[i];
				minDist = distance;
			}
		}
		
		return minTarget;
	},
    
    hurt: function(damage) {},

    fire: function(target) {
    	var pos = this.getPosition();
    	SharedData.gameLayer.addChild( new Bullet( cc.p( pos.x, pos.y ), target ) );
    	
    	this.inCoolDown = true;
    	this.runAction( new cc.Sequence( new cc.DelayTime( SOLDIER_COOLDOWN_TIME ), new cc.CallFunc( this.cooldown, this ) ) );
    },
    
    cooldown: function() {
    	this.inCoolDown = false;
    },
    
    unbuild: function() {
    	SharedData.gameLayer.removeUnit(this);
    	var entity = SharedData.gameLayer.spawnGameObject("Soldier", this.x, this.y);
    	SharedData.gameLayer.selectUnit(entity);
    },
    
    getUIButtons: function() {
    	return [{text:"Deconstruct", callback:"unbuild"}];
    }
});