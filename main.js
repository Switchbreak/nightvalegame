cc.game.onStart = function(){
	var policy = new cc.ResolutionPolicy( cc.ContainerStrategy.ORIGINAL_CONTAINER, cc.ContentStrategy.SHOW_ALL );
    cc.view.setDesignResolutionSize(800, 450, policy);
	cc.view.resizeWithBrowserSize(false);
    //load resources
    cc.LoaderScene.preload(g_resources, function () {
        cc.director.runScene(new TitleScene());
    }, this);
};
cc.game.run();