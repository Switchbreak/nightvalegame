<?xml version="1.0" encoding="UTF-8"?>
<tileset name="UndergroundCity" tilewidth="64" tileheight="64">
 <image source="nightvale.png" width="512" height="448"/>
 <tile id="0">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="20">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="28">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="32">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="33">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="34">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="35">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="36">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="40">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="41">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="42">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="43">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="44">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="48">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="49">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="50">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="51">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
 <tile id="52">
  <properties>
   <property name="Passable" value="True"/>
  </properties>
 </tile>
</tileset>
